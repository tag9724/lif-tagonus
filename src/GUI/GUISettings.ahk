﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk
#Include, ./utils/ObjIndexOf.ahk

#Include, ./settings/SaveSettings.ahk
#Include, ./settings/DefineInventoryPosition.ahk

#Include, ./controllers/EnableAllHotkeys.ahk
#Include, ./controllers/DisableAllHotkeys.ahk


;--------------------------------------------------------;
; Tabs Name & Translation
;--------------------------------------------------------;

MenuTabs            := ["General", "GameKeys", "AdvancedFeatures", "PositioningHelp", "StaticDestacks"]
MenuTabsTranslated  := []

for i, el in MenuTabs {
  MenuTabsTranslated[i] := Lang[ MenuTabs[i] ]
}


OpenSettings(){

    ;--------------------------------------------------------;
    ; Shortcuts
    ;--------------------------------------------------------;

    global ShortcutDestack
    global ShortcutQuickTransferClick
    global ShortcutQuickTombTransfer
    global ShortcutFocusGUI

    global GameInteractionKey
    global GameSecondaryKey
    global GameTertiaryKey

    global ShortcutStartAdvanced
    global ShortcutStopAdvanced
    
    global ShortcutPosHelpLeft
    global ShortcutPosHelpRight
    global ShortcutPosHelpUp
    global ShortcutPosHelpDown
    global ShortcutPosHelpRotLeft
    global ShortcutPosHelpRotRight

    global StaticDestackAmounts
    global ShortcutStaticDestacks

    ;--------------------------------------------------------;
    ; GUI Controls elements
    ;--------------------------------------------------------;

    global SettingsMenuSelectionElement
    global SettingsContainer

    ; General
    global InterfaceLanguageInput
    global DestackShortcutInput
    global QuickTransferClickShortcutInput
    global QuickTombTransferShortcutInput
    global FocusGUIShortcutInput

    ; Game Keys
    global GameInteractionKeyInput
    global GameSecondaryKeyInput
    global GameTertiaryKeyInput

    ; Advanced Features
    global StartAdvancedInput
    global StopAdvancedInput

    ;Positionning Help
    global EnablePositioningHelpInput
    global PosHelpLeftInput
    global PosHelpRightInput
    global PosHelpUpInput
    global PosHelpDownInput
    global PosHelpRotLeftInput
    global PosHelpRotRightInput

    ;Static Destack Amounts
    global StaticDestackAmountInput1
    global StaticDestackAmountInput2
    global StaticDestackAmountInput3
    global StaticDestackAmountInput4
    global StaticDestackAmountInput5
    global StaticDestackAmountInput6
    global StaticDestackAmountInput7
    global StaticDestackAmountInput8
    global StaticDestackAmountInput9
    global StaticDestackAmountInput10

    ;Static Destack Shortcuts
    global ShortcutStaticDestackInput1
    global ShortcutStaticDestackInput2
    global ShortcutStaticDestackInput3
    global ShortcutStaticDestackInput4
    global ShortcutStaticDestackInput5
    global ShortcutStaticDestackInput6
    global ShortcutStaticDestackInput7
    global ShortcutStaticDestackInput8
    global ShortcutStaticDestackInput9
    global ShortcutStaticDestackInput10

    ;--------------------------------------------------------;
    ; Setup Settings window
    ;--------------------------------------------------------;
    
    global AreParametersOpen
    global AreHotkeysEnabled

    global MenuTabs
    global MenuTabsTranslated

    global Lang
    global Languages
    global UserLanguage
  
    global IsPositioningHelpEnabled

    GUIContainerPadding := 180
    GUIElementsSpacing  := 20

    ; Don't open twice the window
    if( AreParametersOpen ){
        return
    }

    AreParametersOpen         := true
    PositioningEnabledChecked := IsPositioningHelpEnabled ? "checked" : ""


    ; Disable hotkeys
    disableAllHotkeys()

    ; Hide Toolbox
    Gui, Toolbox:Hide

    
    ;--------------------------------------------------------;
    ; Setup base UI
    ;--------------------------------------------------------;

    Gui, Settings:Font, s10 c%GUITextColor%, Helvetica
    Gui, Settings:Color, % GUIBackgroundColor


    ;--------------------------------------------------------;
    ; Left Menu
    ;--------------------------------------------------------;

    Gui, Settings:Add, Text, hwndMenuListBoxBgMask x0 y0 w180 h420,
    CtlColors.Attach(MenuListBoxBgMask,GUIBackgroundColor, "")

    Gui, Settings:Add, ListBox, hwndMenuListBox x10 y10 w160 h380 gSettingsMenuTabSelection vSettingsMenuSelectionElement Choose1, % ArrayJoin( "|", MenuTabsTranslated )
    CtlColors.Attach(MenuListBox,GUIInputsBackgroundColor, GUIInputsTextColor)


    ;--------------------------------------------------------;
    ; Container
    ;--------------------------------------------------------;

    ; Gui, Settings:Add, Tab,+Theme -Background x-50 y-50 w550 h450 +Left, General|2
    Gui, Settings:Add, Tab,x0 y0 w560 h420 +Left vSettingsContainer, % ArrayJoin( "|" ,MenuTabs )

    #Include, ./GUI/Settings/tabs/General.ahk
    #Include, ./GUI/Settings/tabs/GameKeys.ahk
    #Include, ./GUI/Settings/tabs/Advanced.ahk
    #Include, ./GUI/Settings/tabs/PositioningHelp.ahk
    #Include, ./GUI/Settings/tabs/StaticDestacks.ahk

    ;--------------------------------------------------------;
    ; Rendering
    ;--------------------------------------------------------;

    Gui, Settings:Font, s8 bold
    Gui, Settings:Add, StatusBar, hwndSettingsStatus, % Lang.disabledWhenOpen

    ; Generated using SmartGUI Creator 4.0
    Gui, Settings:Show, h420 w560, % Lang.parametersTitle

}

;--------------------------------------------------------;
; Events
;--------------------------------------------------------;

#Include, ./GUI/settings/events/SettingsMenuTabSelection.ahk
#Include, ./GUI/settings/events/SettingsGUIClose.ahk
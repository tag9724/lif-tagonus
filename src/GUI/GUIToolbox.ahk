﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

#Include, ./utils/ArrayJoin.ahk
#Include, ./utils/ObjIndexOf.ahk

#Include, ./actions/CloseLauncher.ahk
#Include, ./actions/CloseGame.ahk

#Include, ./settings/OptionsSecondaryXP.ahk
#Include, ./settings/OptionsHerbalist.ahk
#Include, ./settings/OptionsAutoClick.ahk
#Include, ./settings/SetDestackAmount.ahk
#Include, ./settings/SaveAdvancedFeaturesState.ahk

#Include, ./GUI/toolbox/events/EventHeaderAdvancedFeatures.ahk
#Include, ./GUI/toolbox/events/EventHeaderPositioningHelp.ahk
#Include, ./GUI/toolbox/events/EventsChangePositioningHelpAmounts.ahk

#Include, ./GUI/toolbox/utils/GetToolboxHeight.ahk

#Include, ./GUI/toolbox/display/RefreshToolboxUI.ahk

;Load config
IniRead, DestackAmount, Tagonus-user-settings.ini, General, DestackAmount

;Setup Default config
if( !DestackAmount || DestackAmount == "ERROR" ) {
    DestackAmount := 30
}

AdvancedChecked       := AreAdvancedFeaturesToggle ? "checked" : ""
PositioningChecked    := IsPositioningHelpToggle ? "checked" : ""
AdvancedSelectedTabId := ObjIndexOf( AdvancedFeaturesTabsOrder, AdvancedFeaturesTranslations[ AdvancedFeatureSelected ]  )


;--------------------------------------------------------;
; GUI Content
;--------------------------------------------------------;

Gui, Toolbox:Font, s10 c%GUITextColor%, Helvetica
Gui, Toolbox:Color, % GUIBackgroundColor

#Include, ./GUI/toolbox/tabs/MainFeatures.ahk
#Include, ./GUI/toolbox/tabs/PositioningHelp.ahk
#Include, ./GUI/toolbox/tabs/Advanced.ahk


;--------------------------------------------------------;
; Generated using SmartGUI Creator 4.0

RefreshToolboxUI()
Gui, Toolbox:Show, AutoSize,Tagonus v%ScriptVersion%

;--------------------------------------------------------;
;Close application on GUI close

ToolboxGUIClose()
{
    ExitApp
    return
}



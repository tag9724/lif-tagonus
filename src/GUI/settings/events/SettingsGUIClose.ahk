﻿SettingsGUIClose(){
    global AreParametersOpen

    ; Save settings
    SaveSettings()

    ; Destroy GUI
    Gui, Settings:Destroy
    AreParametersOpen := false

    ; Enable Hotkeys again
    EnableAllHotkeys()

    Gui, Toolbox:Show

    return
}
﻿SettingsMenuTabSelection(){
  global MenuTabsTranslated

  GUIControlGet, SettingsMenuSelectionElement
  selected := ObjIndexOf( MenuTabsTranslated, SettingsMenuSelectionElement )

  GuiControl, Choose, SettingsContainer, % selected

  return
}
﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

Gui, Settings:Tab, Advanced

UIY := 10

UIXL := GUIContainerPadding + 10
UIXR := GUIContainerPadding + 210


;--------------------------------------------------------;
;Advanced features

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h100, % Lang.AdvancedFeatures

UIY += 30

Gui, Settings:Font, s8
Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.startAdvanced
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vStartAdvancedInput, % ShortcutStartAdvanced

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.stopAdvanced
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vStopAdvancedInput, % ShortcutStopAdvanced
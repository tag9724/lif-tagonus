﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

Gui, Settings:Tab, GameKeys

UIY := 10

UIXL := GUIContainerPadding + 10
UIXR := GUIContainerPadding + 210


;--------------------------------------------------------;
;Game key mapping

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h130, % Lang.GameKeys

UIY += 30

Gui, Settings:Font, s8
Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.GameInteractionKey
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vGameInteractionKeyInput, % GameInteractionKey

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.GameSecondaryKey
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vGameSecondaryKeyInput, % GameSecondaryKey

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.GameTertiaryKey
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vGameTertiaryKeyInput, % GameTertiaryKey
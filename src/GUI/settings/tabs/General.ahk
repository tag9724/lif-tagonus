﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

Gui, Settings:Tab, General

UIY := 10

UIXL := GUIContainerPadding + 10
UIXR := GUIContainerPadding + 210


;--------------------------------------------------------;
;Interface Language

langIndex := 0
chooseLang := 2
listLanguages := ""

for k, v in Languages {

    langIndex++

    if ( UserLanguage == k) {
        chooseLang := langIndex
    }

    listLanguages := listLanguages "|" v.langName
}

StringTrimLeft, listLanguages, listLanguages, 1


Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h60,% Lang.interface

UIY += 30

Gui, Settings:Font, s8 normal

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20,% Lang.language
Gui, Settings:Add, DropDownList, x%UIXR% y%UIY% w150 Choose%chooseLang% vInterfaceLanguageInput hwndInterfaceLanguageInput,%listLanguages%
CtlColors.Attach( InterfaceLanguageInput, GUIInputsBackgroundColor, GUIInputsTextColor )

UIY += 45

;--------------------------------------------------------;
;General Shortcuts

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h210,% Lang.shortcutList

UIY += 30

Gui, Settings:Font, s8 normal

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20,% Lang.autoDestack
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vDestackShortcutInput, %ShortcutDestack%

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w170 h20,% Lang.quickTransfer
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vQuickTransferClickShortcutInput, %ShortcutQuickTransferClick%

UIY += 45

Gui, Settings:Add, Text, x%UIXL% y%UIY% w170 h20,% Lang.tombTransfer
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vQuickTombTransferShortcutInput, %ShortcutQuickTombTransfer%

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w170 h20,% Lang.inventoryCoords
Gui, Settings:Add, Button, x%UIXR% y%UIY% w150 h20 gDefineInventoryPosition, % Lang.setInventoryCoords

UIY += 45

Gui, Settings:Add, Text, x%UIXL% y%UIY% w180 h20,% Lang.focusToolbox
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vFocusGUIShortcutInput, %ShortcutFocusGUI% 


﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

Gui, Settings:Tab, positioningHelp

UIY := 10

UIXL := GUIContainerPadding + 10
UIXR := GUIContainerPadding + 210


;--------------------------------------------------------;
;Game key mapping

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h70, % Lang.positioningHelp

UIY += 30

Gui, Settings:Font, s8
Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.EnablePositioningHelp
Gui, Settings:Add, CheckBox, x%UIXR% y%UIY% w150 h20 vEnablePositioningHelpInput %PositioningEnabledChecked%,

UIY += 45

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h255, % Lang.shortcutList

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpLeft
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpLeftInput, % ShortcutPosHelpLeft

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpRight
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpRightInput, % ShortcutPosHelpRight

UIY += 45

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpUp
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpUpInput, % ShortcutPosHelpUp

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpDown
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpDownInput, % ShortcutPosHelpDown

UIY += 45

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpRotLeft
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpRotLeftInput, % ShortcutPosHelpRotLeft

UIY += 30

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , % Lang.PosHelpRotRight
Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vPosHelpRotRightInput, % ShortcutPosHelpRotRight

﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk

Gui, Settings:Tab, StaticDestack

UIY := 10

UIXL := GUIContainerPadding + 10
UIXM := GUIContainerPadding + 140
UIXR := GUIContainerPadding + 210


;--------------------------------------------------------;
;Additional Destak

Gui, Settings:Font, s9
Gui, Settings:Add, GroupBox, x%GUIContainerPadding% y%UIY% w370 h340, % Lang.StaticDestacks

;--------------------------------------------------------;
;Destack 1

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 1

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput1 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput1 Range1-999999, % StaticDestackAmounts[1]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput1, % ShortcutStaticDestacks[1]

;--------------------------------------------------------;
;Destack 2

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 2

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput2 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput2 Range1-999999, % StaticDestackAmounts[2]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput2, % ShortcutStaticDestacks[2]

;--------------------------------------------------------;
;Destack 3

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 3

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput3 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput3 Range1-999999, % StaticDestackAmounts[3]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput3, % ShortcutStaticDestacks[3]

;--------------------------------------------------------;
;Destack 4

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 4

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput4 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput4 Range1-999999, % StaticDestackAmounts[4]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput4, % ShortcutStaticDestacks[4]

;--------------------------------------------------------;
;Destack 5

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 5

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput5 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput5 Range1-999999, % StaticDestackAmounts[5]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput5, % ShortcutStaticDestacks[5]

;--------------------------------------------------------;
;Destack 6

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 6

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput6 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput6 Range1-999999, % StaticDestackAmounts[6]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput6, % ShortcutStaticDestacks[6]

;--------------------------------------------------------;
;Destack 7

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 7

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput7 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput7 Range1-999999, % StaticDestackAmounts[7]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput7, % ShortcutStaticDestacks[7]

;--------------------------------------------------------;
;Destack 8

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 8

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput8 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput8 Range1-999999, % StaticDestackAmounts[8]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput8, % ShortcutStaticDestacks[8]

;--------------------------------------------------------;
;Destack 9

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 9

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput9 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput9 Range1-999999, % StaticDestackAmounts[9]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput9, % ShortcutStaticDestacks[9]

;--------------------------------------------------------;
;Destack 10

UIY += 30

Gui, Settings:Font, s8

Gui, Settings:Add, Text, x%UIXL% y%UIY% w160 h20 , Destack 10

Gui, Settings:Add, Edit, x%UIXM% y%UIY% w60 h20 hwndStaticDestackAmountInput10 Number 
Gui, Settings:Add, UpDown, vStaticDestackAmountInput10 Range1-999999, % StaticDestackAmounts[10]

Gui, Settings:Add, Hotkey, x%UIXR% y%UIY% w150 h20 vShortcutStaticDestackInput10, % ShortcutStaticDestacks[10]

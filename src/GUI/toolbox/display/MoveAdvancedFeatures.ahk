﻿MoveAdvancedFeatures(){

  global IsPositioningHelpEnabled
  global IsPositioningHelpToggle

  global AreAdvancedFeaturesToggle

  mainMargin            := 70
  headerHeight          := 20
  positioningHelpHeight := 70


  controls              := [ "AdvancedFeaturesHeader"
                            ,"ToggleAdvancedFeaturesCheckbox"
                            ,"AdvancedFeaturesTabSelectionField"
                            ,"SecondaryXPIntervalActionLabel"
                            ,"SecondaryXPIntervalActionInput"
                            ,"SecondaryXPIntervalAcceptPromptLabel"
                            ,"SecondaryXPIntervalAcceptPromptInput"
                            ,"HerbalistPreparationAmountLabel"
                            ,"HerbalistPreparationAmountInput"
                            ,"HerbalistPreparationCraftTimeLabel"
                            ,"HerbalistPreparationCraftTimeInput"
                            ,"HerbalistRepairTimeLabel"
                            ,"HerbalistRepairTimeInput"
                            ,"AutoClickIntervalLabel"
                            ,"AutoClickIntervalInput"
                            ,"AutoClickTypeLabel"
                            ,"AutoClickTypeInput"
                            ,"AdvancedFeatureRunning"
                            ,"AdvancedFeaturesIterations"
                            ,"AdvancedProgressbar"]


  GuiControlGet,AdvancedFeaturesHeader,Toolbox:Pos
  padding := mainMargin


  if ( IsPositioningHelpEnabled ) {

    ; Add ~20px margin for the PositioningHelp checkbox
    padding += headerHeight

    if ( IsPositioningHelpToggle ){      

      ; Add ~120px margin for the PositioningHelp GroupBox
      padding += positioningHelpHeight
    }
  }


  targetY := padding - AdvancedFeaturesHeaderY

  ; loop over each control
  

  For key, value in controls {

    GuiControlGet, element, Toolbox:Pos, % value
    newPos  := elementY + targetY

    GuiControl, Toolbox:Move, %value%, % "y" newPos

  }

  Gui, Toolbox:Show, AutoSize
  WinSet, Redraw
  
  return

}
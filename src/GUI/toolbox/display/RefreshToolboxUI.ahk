﻿#Include, ./GUI/toolbox/display/TogglePositioningHelp.ahk
#Include, ./GUI/toolbox/display/ToggleAdvancedFeatures.ahk

RefreshToolboxUI(){
  global IsPositioningHelpToggle  
  global AreAdvancedFeaturesToggle

  TogglePositioningHelp( IsPositioningHelpToggle )
  ToggleAdvancedFeatures( AreAdvancedFeaturesToggle )
}
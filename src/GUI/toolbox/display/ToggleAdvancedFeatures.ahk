﻿#Include, ./settings/SaveAdvancedFeaturesState.ahk

ToggleAdvancedFeatures(toggle){
  global AreAdvancedFeaturesToggle

  AreAdvancedFeaturesToggle := toggle
  SaveAdvancedFeaturesState()

  ; It actually debug checkbox ... for some reasons
  Sleep, 1

  if (AreAdvancedFeaturesToggle){
   	GuiControl, Toolbox:Show, AdvancedFeaturesTabSelectionField
   	GuiControl, Toolbox:Show, AdvancedFeatureRunning
   	GuiControl, Toolbox:Show, AdvancedFeaturesIterations
   	GuiControl, Toolbox:Show, AdvancedProgressbar     
  }

  else{   
   	GuiControl, Toolbox:Hide, AdvancedFeaturesTabSelectionField
    GuiControl, Toolbox:Hide, AdvancedFeatureRunning
   	GuiControl, Toolbox:Hide, AdvancedFeaturesIterations
   	GuiControl, Toolbox:Hide, AdvancedProgressbar
  }


  GuiControl, Toolbox:, ToggleAdvancedFeaturesCheckbox, % toggle
  Gui, Toolbox:Show, AutoSize

  WinSet, Redraw  

  return
}
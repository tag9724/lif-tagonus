﻿#Include, ./GUI/toolbox/display/MoveAdvancedFeatures.ahk
#Include, ./settings/SavePositioningHelpState.ahk
#Include, ./controllers/TogglePositioningHelpHotkeys.ahk

TogglePositioningHelp( toggle ){
  global IsPositioningHelpEnabled
  global IsPositioningHelpToggle

  if (toggle != null) {
      GuiControlGet, TogglePositioningHelpCheckbox, Toolbox:
      IsPositioningHelpToggle := TogglePositioningHelpCheckbox
  }

  else {
      IsPositioningHelpToggle := toggle
  }

  ; Debug ...
  Sleep, 1


  if ( IsPositioningHelpEnabled && IsPositioningHelpToggle ){

    GuiControl, Toolbox:Show, TogglePositioningHelpHeader
    GuiControl, Toolbox:Show, TogglePositioningHelpCheckbox

    GuiControl, Toolbox:Show, PositioningHelpLRInput
    GuiControl, Toolbox:Show, PositioningHelpLRLabel

    GuiControl, Toolbox:Show, PositioningHelpUDInput
    GuiControl, Toolbox:Show, PositioningHelpUDLabel

    GuiControl, Toolbox:Show, PositioningHelpRotInput
    GuiControl, Toolbox:Show, PositioningHelpRotLabel
    
    GuiControl, Toolbox:MoveDraw, TogglePositioningHelpHeader, h75

  }

  else {

    if ( !IsPositioningHelpEnabled ){
      GuiControl, Toolbox:Hide, TogglePositioningHelpHeader
      GuiControl, Toolbox:Hide, TogglePositioningHelpCheckbox
    }

    else {
      GuiControl, Toolbox:Show, TogglePositioningHelpHeader
      GuiControl, Toolbox:Show, TogglePositioningHelpCheckbox
    }

    GuiControl, Toolbox:Hide, PositioningHelpLRInput
    GuiControl, Toolbox:Hide, PositioningHelpLRLabel

    GuiControl, Toolbox:Hide, PositioningHelpUDInput
    GuiControl, Toolbox:Hide, PositioningHelpUDLabel

    GuiControl, Toolbox:Hide, PositioningHelpRotInput
    GuiControl, Toolbox:Hide, PositioningHelpRotLabel
    
    GuiControl, Toolbox:MoveDraw, TogglePositioningHelpHeader, h10

  }

  ; Disable/Enable hotkeys
  TogglePositioningHelpHotkeys(IsPositioningHelpToggle)

  ; Update header checkbox and save state
  GuiControl, Toolbox:, TogglePositioningHelpCheckbox, % toggle
  SavePositioningHelpState()

  ; Set the position of advanced features
  MoveAdvancedFeatures()

  return
}
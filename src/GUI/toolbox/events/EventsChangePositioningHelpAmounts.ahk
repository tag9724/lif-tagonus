﻿EventChangePositioningHelpLR(){
  GuiControlGet, PositioningHelpLRInput, Toolbox:
  IniWrite, %PositioningHelpLRInput%, Tagonus-user-settings.ini, PositioningHelp, LRAmount

  return
}

EventChangePositioningHelpUD(){
  GuiControlGet, PositioningHelpUDInput, Toolbox:
  IniWrite, %PositioningHelpUDInput%, Tagonus-user-settings.ini, PositioningHelp, UDAmount

  return
}

EventChangePositioningHelpRot(){
  GuiControlGet, PositioningHelpRotInput, Toolbox:
  IniWrite, %PositioningHelpRotInput%, Tagonus-user-settings.ini, PositioningHelp, RotAmount

  return
}

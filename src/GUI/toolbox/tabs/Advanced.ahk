﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk


Gui, Toolbox:Font, s8

Gui, Toolbox:Add, GroupBox, Section x12 y155 w375 h10 vAdvancedFeaturesHeader, % Lang.advancedFeatures
Gui, Toolbox:Add, CheckBox,xp+0 yp+0 +Right hwndToggleAdvancedFeaturesCheckbox w365 h20 gEventHeaderAdvancedfeatures vToggleAdvancedFeaturesCheckbox %AdvancedChecked%, 
CtlColors.Attach(ToggleAdvancedFeaturesCheckbox,"Trans","")

Gui, Toolbox:Font, s7 normal
Gui, Toolbox:Add, Tab, Section x12 ys+25 w375 h120 gAdvancedFeaturesTabSelection vAdvancedFeaturesTabSelectionField Choose%AdvancedSelectedTabId%, % ArrayJoin( "|",  AdvancedFeaturesTabsOrder )


;--------------------------------------------------------;
;XP Secondary

Gui, Toolbox:Tab, % Lang.featureSecondaryXP

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+35 w180 h20 vSecondaryXPIntervalActionLabel, % Lang.intervalActionKey
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+265 ys+35 w100 h20 +Right gSecondaryXPIntervalActionChange hwndSecondaryXPIntervalActionInput vSecondaryXPIntervalActionInput Number, % SecondaryXPIntervalAction
CtlColors.Attach(SecondaryXPIntervalActionInput,GUIInputsBackgroundColor, GUIInputsTextColor)


Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+60 w180 h20 vSecondaryXPIntervalAcceptPromptLabel, % Lang.intervalAcceptPrompt
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+265 ys+60 w100 h20 +Right gSecondaryXPIntervalAcceptPromptChange hwndSecondaryXPIntervalAcceptPromptInput vSecondaryXPIntervalAcceptPromptInput Number, % SecondaryXPIntervalAcceptPrompt
CtlColors.Attach(SecondaryXPIntervalAcceptPromptInput,GUIInputsBackgroundColor, GUIInputsTextColor)


;--------------------------------------------------------;
;Herbalist craft & repair

Gui, Toolbox:Tab, % Lang.featureHerbalist

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+35 w200 h40 vHerbalistPreparationAmountLabel, % Lang.preparationAmountBeforeRepair
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+265 ys+35 w100 h20 +Right gHerbalistPreparationAmountChange hwndHerbalistPreparationAmountInput vHerbalistPreparationAmountInput Number, % HerbalistPreparationAmount
CtlColors.Attach(HerbalistPreparationAmountInput,GUIInputsBackgroundColor, GUIInputsTextColor)


Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+60 w180 h20 vHerbalistPreparationCraftTimeLabel, % Lang.preparationCraftTime
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+265 ys+60 w100 h20 +Right gHerbalistPreparationCraftTimeChange hwndHerbalistPreparationCraftTimeInput vHerbalistPreparationCraftTimeInput Number, % HerbalistPreparationCraftTime
CtlColors.Attach(HerbalistPreparationCraftTimeInput,GUIInputsBackgroundColor, GUIInputsTextColor)


Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+85 w180 h20 vHerbalistRepairTimeLabel, % Lang.repairTime
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+265 ys+85 w100 h20 +Right gHerbalistRepairTimeChange hwndHerbalistRepairTimeInput vHerbalistRepairTimeInput Number, % HerbalistRepairTime
CtlColors.Attach(HerbalistRepairTimeInput,GUIInputsBackgroundColor, GUIInputsTextColor)


;--------------------------------------------------------;
;Auto click

Gui, Toolbox:Tab, % Lang.featureAutoClick

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+35 w200 h40 vAutoClickIntervalLabel, % Lang.autoClickInterval

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+245 ys+35 w120 h20 +Right gAutoClickIntervalChange vAutoClickIntervalInput hwndAutoClickIntervalInput Number, % AutoClickInterval
CtlColors.Attach(AutoClickIntervalInput,GUIInputsBackgroundColor, GUIInputsTextColor)

AutoClickDropdown := GetAutoClickTypeDropDownList()
AutoClickDropdownChoose := AutoClickDropdown.choose

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, xs+10 ys+60 w180 h20 vAutoClickTypeLabel, % Lang.autoClickType
Gui, Toolbox:Font, s10
Gui, Toolbox:Add, DropDownList, xs+245 ys+60 w120 +Right gAutoClickTypeChange vAutoClickTypeInput hwndAutoClickTypeInput Choose%AutoClickDropdownChoose%, % AutoClickDropdown.list
CtlColors.Attach(AutoClickTypeInput,GUIInputsBackgroundColor, GUIInputsTextColor)


;--------------------------------------------------------;
; Status bar

Gui, Toolbox:Tab,

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, Text, x14 ys+130 w250 h20 vAdvancedFeatureRunning,

Gui, Toolbox:Font, s8 bold
Gui, Toolbox:Add, Text, x275 ys+130 w50 h20 +Right vAdvancedFeaturesIterations,
Gui, Toolbox:Add, Progress, xp+60 yp+0 w50 h15 cacacac Backgrounde1e1e1 vAdvancedProgressbar, 0


;--------------------------------------------------------;
;Events

AdvancedFeaturesTabSelection(){
    global AdvancedFeatureSelected
    global AdvancedFeaturesTranslations

    GuiControlGet, AdvancedFeaturesTabSelectionField

    for k, v in AdvancedFeaturesTranslations {

        if( v == AdvancedFeaturesTabSelectionField ){
            AdvancedFeatureSelected := k

            ;Save state
            SaveAdvancedFeaturesState()

            return
        }
    }

    return
}

GetAutoClickTypeDropDownList(){
    global AutoClickType
    global AdvancedAutoClickTranslations

    index := 0
    chooseType := 2
    clickTypes := ""

    for k, v in AdvancedAutoClickTranslations {
        index++

        if( AutoClickType == k) {
            chooseType := index
        }

        clickTypes := clickTypes "|" v
    }

    StringTrimLeft, clickTypes, clickTypes, 1
    return { list : clickTypes, choose: chooseType }
}

﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk


;--------------------------------------------------------;
;Auto Destack

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, GroupBox, x12 y8 w115 h55 ,
Gui, Toolbox:Add, Text, x22 y8 w70 h20,% Lang.destackAmount
Gui, Toolbox:Add, Text, x70 y8 w45 h20 +Right vDestackAmountLabel, %DestackAmount%

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, x20 y28 w68 h25 hwndDestackAmountInput Number 
Gui, Toolbox:Add, UpDown, vDestackAmountInput  Range1-999999, %DestackAmount%
CtlColors.Attach(DestackAmountInput,GUIInputsBackgroundColor, GUIInputsTextColor)

Gui, Toolbox:Font, s8
GuiControl, Move, DestackAmountInput, w16
Gui, Toolbox:Add, Button, x+4 y27 w28 h27 default gSetDestackAmount, ✔


;--------------------------------------------------------;
;Rage quit

Gui, Toolbox:Font, s8
Gui, Toolbox:Add, GroupBox, x137 y8 w180 h55,% Lang.rageQuit

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Button, x147 y28 w80 h28 hwndCloseLauncherButton gCloseLauncher,% Lang.quitLauncher
Gui, Toolbox:Add, Button, x227 y28 w80 h28 gCloseGame,% Lang.quitGame


;--------------------------------------------------------;
;Parameters

Gui, Toolbox:Font, s16
Gui, Toolbox:Add, Button, x327 y14 w60 h49 gOpenSettings, ⚙
﻿#NoEnv
#Include ./libs/Class_CtlColors.ahk


Gui, Toolbox:Font, s8

Gui, Toolbox:Add, GroupBox, Section x12 y70 w375 h75 vTogglePositioningHelpHeader, % Lang.positioningHelp
Gui, Toolbox:Add, CheckBox,xp+0 yp+0 +Right hwndTogglePositioningHelpCheckbox w365 h20 gEventHeaderPositioningHelp vTogglePositioningHelpCheckbox %PositioningChecked%, 
CtlColors.Attach(TogglePositioningHelpCheckbox,"Trans","")

;--------------------------------------------------------;
; Left, Right

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+10 ys+25 w100 h20 +Right Number hwndPositioningHelpLRInput gEventChangePositioningHelpLR vPositioningHelpLRInput, % PositioningHelpLRAmount
CtlColors.Attach(PositioningHelpLRInput,GUIInputsBackgroundColor, GUIInputsTextColor)

Gui, Toolbox:Font, s7
Gui, Toolbox:Add, Text, xs+10 ys+50 w100 h20 Center vPositioningHelpLRLabel, % Lang.PosHelpLeft " / " Lang.PosHelpRight


;--------------------------------------------------------;
; Up Down

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+135 ys+25 w100 h20 +Right Number hwndPositioningHelpUDInput gEventChangePositioningHelpUD vPositioningHelpUDInput, % PositioningHelpUDAmount
CtlColors.Attach(PositioningHelpUDInput,GUIInputsBackgroundColor, GUIInputsTextColor)

Gui, Toolbox:Font, s7
Gui, Toolbox:Add, Text, xs+135 ys+50 w100 h20 Center vPositioningHelpUDLabel, % Lang.PosHelpUp " / " Lang.PosHelpDown


;--------------------------------------------------------;
; Rotation

Gui, Toolbox:Font, s10
Gui, Toolbox:Add, Edit, xs+260 ys+25 w100 h20 +Right Number hwndPositioningHelpRotInput gEventChangePositioningHelpRot vPositioningHelpRotInput, % PositioningHelpRotAmount
CtlColors.Attach(PositioningHelpRotInput,GUIInputsBackgroundColor, GUIInputsTextColor)

Gui, Toolbox:Font, s7
Gui, Toolbox:Add, Text, xs+260 ys+50 w100 h20 Center vPositioningHelpRotLabel, % Lang.PosHelpRot

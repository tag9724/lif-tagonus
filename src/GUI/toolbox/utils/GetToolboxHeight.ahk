﻿GetToolboxHeight(){
  global AreAdvancedFeaturesToggle

  height := 105

  if( AreAdvancedFeaturesToggle ){
    height += 185
  }

  return "h" + height

}
﻿Languages["en-EN"] := { langName : "English"

;Main GUI
,destackAmount: "Destack"
,rageQuit: "Rage Quit"
,quitLauncher: "Launcher"
,quitGame: "The game"
,parameters: "Parameters"

;Advanced features Tabs
,featureHerbalist: "Herbalist"
,featureSecondaryXP: "Secondary XP"
,featureAutoClick: "Auto Click"
,intervalActionKey: "Interval action key ( ms )"
,intervalAcceptPrompt: "Interval accept placement ( ms )"
,preparationAmountBeforeRepair: "Amount of preparations before repair"
,preparationCraftTime: "Preparation craft time ( ms )"
,repairTime: "Needed time to repair ( ms )"
,autoClickInterval: "Click interval ( ms )"
,autoClickType: "Click type"
,runningFeaturePrefix: "Running"
,actionKey: "Action key"
,secondaryKey: "Secondary key"
,tertiaryKey: "Tertiary key"
,dblClick: "Double Click"

;Parameters Title + Interface
,parametersTitle: "Parameters" 
,interface: "Interface"
,language: "Language"

;Parameters General
,shortcutList: "Shortcuts list"
,autoDestack: "Auto Destack"
,quickTransfer: "Quick transfer ( ctrl + click )"
,tombTransfer: "Quick Tomb transfer"
,inventoryCoords: "Inventory position"
,setInventoryCoords: "Set inventory position"
,focusToolbox: "Focus Tagonus toolbox"

;Inventory position update
,inventoryPositionUpdated: "Inventory position updated succefully"
,inventoryPositionCannotUpdate: "Inventory position not updated`nYou must be focused on the game window when clicking !"

;Settings Menus
,General: "General"
,AdvancedFeatures: "Advanced features"
,GameKeys: "In game keys"
,PositioningHelp: "Positioning help"
,StaticDestacks: "Static destack"

;Parameters Advanced
,GameInteractionKey: "game action key"
,GameSecondaryKey: "game secondary key"
,GameTertiaryKey: "game tertiary key"
,startAdvanced: "Start selected feature"
,stopAdvanced: "Stop currently running feature"


;Positioning Help
,EnablePositioningHelp: "Enable positioning help"
,PosHelpLeft: "Left"
,PosHelpRight: "Right"
,PosHelpUp: "Up"
,PosHelpDown: "Down"
,PosHelpRot: "Rotation"
,PosHelpRotLeft: "Rotation left"
,PosHelpRotRight: "Rotation right"


;Parameters Messages
,requireStopAdvanced: "Warning !`r`rYou need to specify a shortcut to stop a running advanced feature.`r`rAdvanced features will remain disabled until you set a shorcut to stop them."

;Parameters footer
,disabledWhenOpen: "All shortcuts are disabled when this window is open"}
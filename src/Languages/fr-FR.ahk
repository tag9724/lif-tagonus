﻿Languages["fr-FR"] := { langName : "Français"

;Main GUI
,destackAmount: "Destack"
,rageQuit: "Rage Quit"
,quitLauncher: "Launcher"
,quitGame: "Le jeu"
,parameters: "Paramètres"

;Advanced features Tabs
,featureHerbalist: "Herboristerie"
,featureSecondaryXP: "XP Meubles"
,featureAutoClick: "Auto Click"
,intervalActionKey: "Interval touche d'action ( ms )"
,intervalAcceptPrompt: "Interval accepter placement ( ms )"
,preparationAmountBeforeRepair: "Nombre de préparations avant réparation"
,preparationCraftTime: "Temps pour une préparation ( ms )"
,repairTime: "Temps de réparation ( ms )"
,autoClickInterval: "Click interval ( ms )"
,autoClickType: "Type de click"
,runningFeaturePrefix: "En cours"
,actionKey: "Touche d'action"
,secondaryKey: "Touche secondaire"
,tertiaryKey: "Touche tertiaire"
,dblClick: "Double Click"

;Parameters Title + Interface
,parametersTitle: "Paramètres" 
,interface: "Interface"
,language: "Langue"

;Parameters General
,shortcutList: "Raccourcis claviers"
,autoDestack: "Destack automatique"
,quickTransfer: "Transfer rapide ( ctrl + click )"
,tombTransfer: "Transfer rapide depuis tombe"
,inventoryCoords: "Position de l'inventaire"
,setInventoryCoords: "Définir emplacement"
,focusToolbox: "Mettre Tagonus en premier plan"

;Inventory position update
,inventoryPositionUpdated: "Emplacement mis à jour avec succès"
,inventoryPositionCannotUpdate: "Emplacement non mis à jour `nVous devez impérativement cliquer sur la fenètre de jeu"

;Settings Menus
,General: "Général"
,AdvancedFeatures: "Fonctionalités avancées"
,GameKeys: "Touches du jeu"
,PositioningHelp: "Aide au placement"
,StaticDestacks: "Destack statiques"

;Parameters Advanced
,GameInteractionKey: "Touche d'action du jeu"
,GameSecondaryKey: "Touche secondaire ( optionnel )"
,GameTertiaryKey: "Touche tertiaire ( optionnel )"
,startAdvanced: "Lancer la maccro sélectionnée"
,stopAdvanced: "Arrêter la maccro en cours"

;Positioning Help
,EnablePositioningHelp: "Activer l'aide au placement"
,PosHelpLeft: "Gauche"
,PosHelpRight: "Droite"
,PosHelpUp: "Haut"
,PosHelpDown: "Bas"
,PosHelpRot: "Rotation"
,PosHelpRotLeft: "Rotation gauche"
,PosHelpRotRight: "Rotation droite"

;Parameters Messages
,requireStopAdvanced: "Attention !`r`rVous n'avez pas définis de raccouris pour stopper les fonctionnalités avancées`r`rCelle-ci serons désactivées jusqu'a que vous définissiez un raccourci"

;Parameters footer
,disabledWhenOpen: "Attention tous les raccourcis claviers sont désactivés quand cette fenêtre est ouverte"}

﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
DetectHiddenWindows, On
SetControlDelay, 45

;--------------------------------------------------------;

;Current Tagonus Version
ScriptVersion = 1.1.1

;Default language
DefaultLanguage := "fr-FR"

;--------------------------------------------------------;
; General Settings

FirstQuickActionTimeout  := 250
QuickTransferInterval    := 65

GUIBackgroundColor       := "f7f5f4"
GUITextColor             := "2e3440"
GUIInputsBackgroundColor := "f8f8f8"
GUIInputsTextColor       := "181818"


;--------------------------------------------------------;
;General global variables

AreHotkeysEnabled := false
IsDestackActive   := false
IsTransferActive  := false

; Advanced Features

IniRead, AreAdvancedFeaturesToggle, Tagonus-user-settings.ini, States, AdvancedFeaturesToggle
IniRead, AdvancedFeatureSelected, Tagonus-user-settings.ini, States, AdvancedFeatureSelected

if( AreAdvancedFeaturesToggle == "ERROR") {
    AreAdvancedFeaturesToggle := false
}

if( AdvancedFeatureSelected == "ERROR") {
    AdvancedFeatureSelected := "autoClick"
}

AdvancedfeatureRunID := 0
RunningHotkeys := { }

; Positioning Help

IniRead, IsPositioningHelpEnabled, Tagonus-user-settings.ini, PositioningHelp, PositioningHelpEnabled

if( IsPositioningHelpEnabled == "ERROR") {
    IsPositioningHelpEnabled := false
}

IniRead, IsPositioningHelpToggle, Tagonus-user-settings.ini, States, PositioningHelpToggle

if( IsPositioningHelpToggle == "ERROR") {
    IsPositioningHelpToggle := false
}

ArePosHelpHotkeysEnabled := { PosHelpLeft : false
                             ,PosHelpRight : false
                             ,PosHelpUp : false
                             ,PosHelpDown : false
                             ,PosHelpRotLeft : false
                             ,PosHelpRotRight : false}


IniRead, PositioningHelpLRAmount, Tagonus-user-settings.ini, PositioningHelp, LRAmount
IniRead, PositioningHelpUDAmount, Tagonus-user-settings.ini, PositioningHelp, UDAmount
IniRead, PositioningHelpRotAmount, Tagonus-user-settings.ini, PositioningHelp, RotAmount

if ( !PositioningHelpLRAmount || PositioningHelpLRAmount == "ERROR" ) {
    PositioningHelpLRAmount := 1
}

if ( !PositioningHelpUDAmount || PositioningHelpUDAmount == "ERROR" ) {
    PositioningHelpUDAmount := 1
}

if ( !PositioningHelpRotAmount || PositioningHelpRotAmount == "ERROR" ) {
    PositioningHelpRotAmount := 90
}


;--------------------------------------------------------;
;---------------- Language / Translations ---------------;
;--------------------------------------------------------;

;Setup Lang object
Languages := { }

;Load all availables languages
#Include, ./languages/fr-FR.ahk
#Include, ./languages/en-EN.ahk

;Setup messages set
IniRead, UserLanguage, Tagonus-user-settings.ini, General, Language

if( !UserLanguage || UserLanguage == "ERROR") {
    Lang         := Languages[ DefaultLanguage ]
    UserLanguage := DefaultLanguage
}

else {
    Lang         := Languages[ UserLanguage ]
}


;Load Advanced features translations
AdvancedFeaturesTranslations  := { autoClick: Lang.featureAutoClick
                                 , secondaryXP: Lang.featureSecondaryXP
                                 , herbalist: Lang.featureHerbalist}

AdvancedAutoClickTranslations := { click: "Click"
                                 , ctrlClick: "Ctrl+Click"
                                 , altClick: "Alt+Click"
                                 , dblClick: Lang.dblClick
                                 , actionKey: Lang.actionKey
                                 , secondaryKey: Lang.secondaryKey
                                 , tertiaryKey: Lang.tertiaryKey}

AdvancedFeaturesTabsOrder     := [ Lang.featureAutoClick
                                 , Lang.featureHerbalist
                                 , Lang.featureSecondaryXP]


;--------------------------------------------------------;
;------------------ User defined hotkeys ----------------;
;--------------------------------------------------------;

;Load Quick transfer config
IniRead, ShortcutQuickTransferClick, Tagonus-user-settings.ini, Shortcuts, QuickTransferClick

if( ShortcutQuickTransferClick == "ERROR" ) {
    ShortcutQuickTransferClick = F6
}

;--------------------------------------------------------;

;Load Quick Tomb transfer config
IniRead, ShortcutQuickTombTransfer, Tagonus-user-settings.ini, Shortcuts, QuickTombTransfer
IniRead, InventoryCoords, Tagonus-user-settings.ini, General, InventoryCoords

if( !InventoryCoords || InventoryCoords == "ERROR" ) {
    InventoryCoords := "0,0"
}

InventoryCoords := StrSplit(InventoryCoords, ",")

;--------------------------------------------------------;
;Load Destack config

IniRead, ShortcutDestack, Tagonus-user-settings.ini, Shortcuts, Destack

if( ShortcutDestack == "ERROR" ) {
    ShortcutDestack = ²
}

;--------------------------------------------------------;
;Load Focus Tagonus hotkey

IniRead, ShortcutFocusGUI, Tagonus-user-settings.ini, Shortcuts, GUIFocus

if( ShortcutFocusGUI == "ERROR" ) {
    ShortcutFocusGUI = F5
}

;--------------------------------------------------------;
;Load Advanced features hotkeys

IniRead, ShortcutStartAdvanced, Tagonus-user-settings.ini, Shortcuts, StartAdvanced
IniRead, ShortcutStopAdvanced, Tagonus-user-settings.ini, Shortcuts, StopAdvanced

if( ShortcutStartAdvanced == "^F1" || ShortcutStartAdvanced == "ERROR" ) {
    ShortcutStartAdvanced = F1
}

if( ShortcutStopAdvanced == "^F2" || ShortcutStopAdvanced == "ERROR" ) {
    ShortcutStopAdvanced = F2
}

;--------------------------------------------------------;

;Load User Game action key

IniRead, GameInteractionKey, Tagonus-user-settings.ini, General, GameInteractionKey

if( !GameInteractionKey || GameInteractionKey == "ERROR" ) {
    GameInteractionKey = E
}

;Load User Game secondary key

IniRead, GameSecondaryKey, Tagonus-user-settings.ini, General, GameSecondaryKey

if( !GameSecondaryKey || GameSecondaryKey == "ERROR" ) {
    GameSecondaryKey = False
}

;Load User Game tertiary key

IniRead, GameTertiaryKey, Tagonus-user-settings.ini, General, GameTertiaryKey

if( !GameTertiaryKey || GameTertiaryKey == "ERROR" ) {
    GameTertiaryKey = False
}


;--------------------------------------------------------;
;Load Advanced feature settings - Secondary XP

IniRead, SecondaryXPIntervalAction, Tagonus-user-settings.ini, SecondaryXP, IntervalAction
IniRead, SecondaryXPIntervalAcceptPrompt, Tagonus-user-settings.ini, SecondaryXP, IntervalAcceptPrompt

if( !SecondaryXPIntervalAction || SecondaryXPIntervalAction == "ERROR" ) {
    SecondaryXPIntervalAction = 1800
}

if( !SecondaryXPIntervalAcceptPrompt || SecondaryXPIntervalAcceptPrompt == "ERROR" ) {
    SecondaryXPIntervalAcceptPrompt = 500
}

;--------------------------------------------------------;
;Load Advanced feature settings - Herbalist

IniRead, HerbalistPreparationAmount, Tagonus-user-settings.ini, Herbalist, PreparationAmount
IniRead, HerbalistPreparationCraftTime, Tagonus-user-settings.ini, Herbalist, PreparationCraftTime
IniRead, HerbalistRepairTime, Tagonus-user-settings.ini, Herbalist, RepairTime

if( !HerbalistPreparationAmount || HerbalistPreparationAmount == "ERROR" ) {
    HerbalistPreparationAmount = 40
}

if( !HerbalistPreparationCraftTime || HerbalistPreparationCraftTime == "ERROR" ) {
    HerbalistPreparationCraftTime = 1000
}

if( !HerbalistRepairTime || HerbalistRepairTime == "ERROR" ) {
    HerbalistRepairTime = 4000
}
;--------------------------------------------------------;
;Load Advanced feature settings - AutoClick

IniRead, AutoClickInterval, Tagonus-user-settings.ini, AutoClick, Interval
IniRead, AutoClickType, Tagonus-user-settings.ini, AutoClick, Type

if( !AutoClickInterval || AutoClickInterval == "ERROR" ) {
    AutoClickInterval = 1000
}

if( !AutoClickType || AutoClickType == "ERROR" ) {
    AutoClickType := "actionKey"
}


;--------------------------------------------------------;
;Load Positioning Help hotkeys

IniRead, ShortcutPosHelpLeft, Tagonus-user-settings.ini, Shortcuts, PosHelpLeft
IniRead, ShortcutPosHelpRight, Tagonus-user-settings.ini, Shortcuts, PosHelpRight

if( ShortcutPosHelpLeft == "ERROR" ) {
    ShortcutPosHelpLeft = Left
}

if( ShortcutPosHelpRight == "ERROR" ) {
    ShortcutPosHelpRight = Right
}

IniRead, ShortcutPosHelpUp, Tagonus-user-settings.ini, Shortcuts, PosHelpUp
IniRead, ShortcutPosHelpDown, Tagonus-user-settings.ini, Shortcuts, PosHelpDown

if( ShortcutPosHelpUp == "ERROR" ) {
    ShortcutPosHelpUp = Up
}

if( ShortcutPosHelpDown == "ERROR" ) {
    ShortcutPosHelpDown = Down
}


IniRead, ShortcutPosHelpRotLeft, Tagonus-user-settings.ini, Shortcuts, PosHelpRotLeft
IniRead, ShortcutPosHelpRotRight, Tagonus-user-settings.ini, Shortcuts, PosHelpRotRight

if( ShortcutPosHelpRotLeft == "ERROR" ) {
    ShortcutPosHelpRotLeft = PgUp
}

if( ShortcutPosHelpRotRight == "ERROR" ) {
    ShortcutPosHelpRotRight = PgDn
}


;--------------------------------------------------------;
;Load Static destacks hotkeys

StaticDestackAmounts           := [ 1, 5, 10, 15, 45, 85, 100, 300, 380, 500 ]
ShortcutStaticDestacks         := [ ]
AreStaticDestackHotkeysEnabled := [ ]

Loop, 10 {

    IniRead, staticDestackAmount,   Tagonus-user-settings.ini, StaticDestackAmounts, StaticDestack%A_Index%
    IniRead, shortcutStaticDestack, Tagonus-user-settings.ini, Shortcuts, StaticDestack%A_Index%

    if(staticDestackAmount && staticDestackAmount != "ERROR"){
        StaticDestackAmounts[A_Index] := staticDestackAmount
    }


    if( !shortcutStaticDestack || shortcutStaticDestack == "ERROR" ) {
        ShortcutStaticDestacks[A_Index] = False
    }

    else {
        ShortcutStaticDestacks[A_Index] := shortcutStaticDestack
    }
}


;--------------------------------------------------------;
;Enable All hotkeys

#Include, ./controllers/EnableAllHotkeys.ahk
EnableAllHotkeys()

;--------------------------------------------------------;
;------------------------ GUI ---------------------------;
;--------------------------------------------------------;

#Include, ./GUI/GUIToolbox.ahk
#Include, ./GUI/GUISettings.ahk

;--------------------------------------------------------;
;------------------------ DEBUG -------------------------;
;--------------------------------------------------------;

; #Include, ./debug/getCenterRelativeCoords.ahk

﻿#Include, ./utils/IsGameWindowActive.ahk

SendDestack(amount, shortcut)
{
    global AreHotkeysEnabled
    global IsDestackActive
    global DestackInputDelay
    global FirstQuickActionTimeout

    if( !IsDestackActive && AreHotkeysEnabled ) {
        IsDestackActive := true
        
        ;Get window size
        WinGetPos, wx, wy, width, height, A
        
        winSizeX := ( width / 2 )
        winSizeY := ( height / 2 )
        
        ;Start destack loop
        loop {
            if( !IsGameWindowActive() || !GetKeyState( shortcut ) ) {
                break
            }
            
            ;Save initial mouse position
            MouseGetPos, MousePosX, MousePosY
            
            ;Display Destack popup
            ControlFocus, , ahk_exe cm_client.exe
            ControlSend, , {ShiftDown}, ahk_exe cm_client.exe
            
            MouseClick, left
            
            ControlFocus, , ahk_exe cm_client.exe
            ControlSend, , {ShiftUp}, ahk_exe cm_client.exe
            
            ;Focus the amount box            
            MouseClick, left, winSizeX + 260, winSizeY - 28.5
            MouseClick, left, winSizeX + 260, winSizeY - 28.5
            
            ;Enter Destack amount
            Send, {Text}%amount%
            
            ;Validate Input
            MouseClick, left, winSizeX - 11, winSizeY + 42.5
            
            ;Reset Mouse position
            MouseMove, MousePosX, MousePosY
            
            ;First iteration got a delay
            if( !isFirstIterationOver ) {
                isFirstIterationOver = 1
                Sleep, %FirstQuickActionTimeout%
            }
        }
        
        IsDestackActive := false
    }
    
    return
}

﻿SendGameInteraction(){
  global GameInteractionKey

  ControlFocus,,ahk_exe cm_client.exe
  ControlSend,,{Control down}%GameInteractionKey%{Control up},ahk_exe cm_client.exe
  
  return
}

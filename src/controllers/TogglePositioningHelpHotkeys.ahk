﻿#Include, ./hotkeys/HotkeysPositioningHelp.ahk

TogglePositioningHelpHotkeys( toggle ){

    global ArePosHelpHotkeysEnabled
    global IsPositioningHelpEnabled
    global IsPositioningHelpToggle

    global ShortcutPosHelpLeft
    global ShortcutPosHelpRight
    global ShortcutPosHelpUp
    global ShortcutPosHelpDown
    global ShortcutPosHelpRotLeft
    global ShortcutPosHelpRotRight

    shortcuts := {  PosHelpLeft: ShortcutPosHelpLeft
                   ,PosHelpRight: ShortcutPosHelpRight
                   ,PosHelpUp: ShortcutPosHelpUp
                   ,PosHelpDown: ShortcutPosHelpDown
                   ,PosHelpRotLeft: ShortcutPosHelpRotLeft
                   ,PosHelpRotRight: ShortcutPosHelpRotRight}

    ; Disabled outside game window
    Hotkey, IfWinActive, ahk_exe cm_client.exe

    ; Enable hotkeys
    if ( toggle && ( IsPositioningHelpEnabled && IsPositioningHelpToggle ) ) {
 
      for key, value in ArePosHelpHotkeysEnabled {

        if ( !value ){
          shortcut                      := shortcuts[ key ]
          function                      := "Hotkey"   key

          if ( shortcut && shortcut != "ERROR" ){
            ArePosHelpHotkeysEnabled[key] := true
            Hotkey, %shortcut%, %function%, on
          }
        }

      }

      return
    }

    ; Disable Hotkeys
    if ( !toggle ) {

      for key, value in ArePosHelpHotkeysEnabled {

        if ( value ){
          shortcut                      := shortcuts[ key ]
          function                      := "Hotkey"   key

          ArePosHelpHotkeysEnabled[key] := false
          Hotkey, %shortcut%, %function%, off
        }

      }

      return
    }

    return
}
﻿#Include, ./hotkeys/HotkeysAdvancedFeatures.ahk
#Include, ./hotkeys/HotkeysDestack.ahk
#Include, ./hotkeys/HotkeyFocusGUI.ahk
#Include, ./hotkeys/HotkeyQuickTombTransfer.ahk
#Include, ./hotkeys/HotkeyQuickTransferClick.ahk

#Include, ./controllers/TogglePositioningHelpHotkeys.ahk

DisableAllHotkeys(){

  global AreHotkeysEnabled
  global RunningHotkeys

  global ShortcutDestack    
  global ShortcutQuickTransferClick
  global ShortcutQuickTombTransfer    
  global ShortcutFocusGUI

  global ShortcutStartAdvanced
  global ShortcutStopAdvanced

  global ShortcutStaticDestacks
  global AreStaticDestackHotkeysEnabled

  if( AreHotkeysEnabled ){

    ;Disable hotkeys
    AreHotkeysEnabled := false

    ; Can be trigger outside game window
    Hotkey, IfWinActive,

    if( RunningHotkeys.focusGUI ){
      RunningHotkeys.focusGUI := false
      Hotkey, %ShortcutFocusGUI%, HotkeyFocusGUI, off
    }

    if( RunningHotkeys.advanced ){
      RunningHotkeys.advanced := false

      Hotkey, %ShortcutStartAdvanced%, HotkeyAdvancedFeaturesStart, off
      Hotkey, %ShortcutStopAdvanced%, HotkeyAdvancedFeaturesStop, off
    }

    if( RunningHotkeys.destack ){
      RunningHotkeys.destack := false
      Hotkey, %ShortcutDestack%, HotkeyDestack, off
    }

    if( RunningHotkeys.quickTransfer ){
      RunningHotkeys.quickTransfer := false
      Hotkey, %ShortcutQuickTransferClick%, HotkeyQuickTransferClick, off
    }

    if( RunningHotkeys.lootTomb ){
      RunningHotkeys.lootTomb := false
      Hotkey, %ShortcutQuickTombTransfer%, HotkeyQuickTombTransfer, off
    }

    ; Force Stop currently running advanced feature
    HotkeyAdvancedFeaturesStop()


    ; Disable Static Destack      
    Loop, 10 {
      
      if( AreStaticDestackHotkeysEnabled[A_Index] ) {
        AreStaticDestackHotkeysEnabled[A_Index] := False
        Hotkey, % ShortcutStaticDestacks[A_Index], HotkeyStaticDestack%A_Index%, off
      }

    }

    ; Disable Positioning Help
    TogglePositioningHelpHotkeys(false)

  }


  return
}
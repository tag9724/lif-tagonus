﻿#Include, ./hotkeys/HotkeysAdvancedFeatures.ahk
#Include, ./hotkeys/HotkeysDestack.ahk
#Include, ./hotkeys/HotkeyFocusGUI.ahk
#Include, ./hotkeys/HotkeyQuickTombTransfer.ahk
#Include, ./hotkeys/HotkeyQuickTransferClick.ahk

#Include, ./controllers/TogglePositioningHelpHotkeys.ahk

EnableAllHotkeys(){
    global AreHotkeysEnabled
    global RunningHotkeys

    global ShortcutDestack    
    global ShortcutQuickTransferClick
    global ShortcutQuickTombTransfer    
    global ShortcutFocusGUI
    
    global ShortcutStartAdvanced
    global ShortcutStopAdvanced

    global ShortcutStaticDestacks
    global AreStaticDestackHotkeysEnabled

    if( !AreHotkeysEnabled ){

      AreHotkeysEnabled := true

      ; Can be trigger outside game window
      Hotkey, IfWinActive,

      if( ShortcutFocusGUI && ShortcutFocusGUI != "ERROR" ){
        RunningHotkeys.focusGUI := true
        Hotkey, %ShortcutFocusGUI%, HotkeyFocusGUI, on
      }

      if( ShortcutStartAdvanced && ShortcutStopAdvanced && ShortcutStartAdvanced != "ERROR" && ShortcutStopAdvanced != "ERROR" ){
        RunningHotkeys.advanced := true

        Hotkey, %ShortcutStartAdvanced%, HotkeyAdvancedFeaturesStart, on
        Hotkey, %ShortcutStopAdvanced%, HotkeyAdvancedFeaturesStop, on
      }

      if( ShortcutDestack && ShortcutDestack != "ERROR" ){
        RunningHotkeys.destack := true
        Hotkey, %ShortcutDestack%, HotkeyDestack, on
      }

      if( ShortcutQuickTransferClick && ShortcutQuickTransferClick != "ERROR" ){
        RunningHotkeys.quickTransfer := true
        Hotkey, %ShortcutQuickTransferClick%, HotkeyQuickTransferClick, on
      }

      if( ShortcutQuickTombTransfer && ShortcutQuickTombTransfer != "ERROR" ) {
        RunningHotkeys.lootTomb := true
        Hotkey, %ShortcutQuickTombTransfer%, HotkeyQuickTombTransfer, on
      }

      ; Enable Static destack hotkeys

      Loop, 10 {
        
        if( ShortcutStaticDestacks[A_Index] && ShortcutStaticDestacks[A_Index] != "ERROR" ) {
          AreStaticDestackHotkeysEnabled[A_Index] := True
          Hotkey, % ShortcutStaticDestacks[A_Index], HotkeyStaticDestack%A_Index%, on
        }

      }


      TogglePositioningHelpHotkeys(true)

    }

    return

}
﻿HotkeyGetGameRelativeCoords()
{
    if( IsGameWindowActive() ) {
        
        ; Get Windows coords and size
        WinGetPos, wx, wy, width, height, A
        
        ; Get mouse position relative to game window
        MouseGetPos, mx, my
        
        ; MouseMove, wx + (width / 2)
        
        ; MsgBox, Hello X%mx% Y%my% - Width %width% Height %height%
        
        mouseX := mx - ( width / 2 )
        mouseY := my - ( height / 2 )
        
        
        
        MsgBox, X %mouseX% Y %mouseY%
    }
}

HotkeyGetGameRelativeCoordsTest()
{
    if( IsGameWindowActive() ) {
        
        ; Get Windows coords and size
        WinGetPos, wx, wy, width, height, A
        
        ; Get mouse position relative to game window
        MouseGetPos, mx, my
        
        winSizeX := ( width / 2 )
        winSizeY := ( height / 2 )
        
        MouseMove, winSizeX - 11, winSizeY + 42.5
    }
}

Hotkey, F9, HotkeyGetGameRelativeCoords, on
Hotkey, F10, HotkeyGetGameRelativeCoordsTest, on
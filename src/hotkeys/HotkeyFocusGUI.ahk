HotkeyFocusGUI(){

  WinGet, winPID , PID , A
  WinGet, toolboxPID, PID, ahk_class AutoHotkeyGUI

  ;GUI is currently focused, hide it
  if( winPID == toolboxPID){
    Gui, Toolbox:Hide
  }

  ;GUI is not active, display it
  else {
    Gui, Toolbox:Show
  }
}
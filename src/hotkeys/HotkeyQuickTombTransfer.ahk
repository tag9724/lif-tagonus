﻿#Include, ./utils/IsGameWindowActive.ahk

HotkeyQuickTombTransfer()
{
    global IsTransferActive
    global AreHotkeysEnabled
    global QuickTransferInterval
    global FirstQuickActionTimeout
    global ShortcutQuickTombTransfer
    global InventoryCoords

    if( !IsTransferActive && AreHotkeysEnabled ) {
        
        IsTransferActive := true

        ;Get window size
        WinGetPos, wx, wy, width, height, A
        
        winSizeX := ( width / 2 )
        winSizeY := ( height / 2 )

        loop {
            
            if( !IsGameWindowActive() || !GetKeyState( ShortcutQuickTombTransfer ) ) {
                break
            }

            ;Save initial mouse position
            MouseGetPos, MousePosX, MousePosY

            ;Execute Mouse down + drag to inventory
            MouseClickDrag, Left, MousePosX, MousePosY, winSizeX + InventoryCoords[1], winSizeY + InventoryCoords[2], 0

            ;Reset Mouse position
            MouseMove, MousePosX, MousePosY
            
            ;First iteration got a delay
            if( !isFirstIterationOver ) {
                isFirstIterationOver = 1
                Sleep, %FirstQuickActionTimeout%
            }
            
            ;Debounce, too fast really
            else {
                Sleep, %QuickTransferInterval%
            }
        }
        
        IsTransferActive := false
    }
    
    return
}
﻿#Include, ./actions/SendTransferCMD.ahk
#Include, ./utils/IsGameWindowActive.ahk

HotkeyQuickTransferClick()
{
    global IsTransferActive
    global AreHotkeysEnabled
    global ShortcutQuickTransferClick
    global QuickTransferInterval
    global FirstQuickActionTimeout
    
    if( !IsTransferActive && AreHotkeysEnabled ) {
        
        IsTransferActive := true
        
        loop {
            
            if( !IsGameWindowActive() || !GetKeyState( ShortcutQuickTransferClick ) ) {
                break
            }
            
            ;Execute Ctrl + Click
            SendTransferCMD()
            
            ;First iteration got a delay
            if( !isFirstIterationOver ) {
                isFirstIterationOver = 1
                Sleep, %FirstQuickActionTimeout%
            }
            
            ;Debounce, too fast really
            else {
                Sleep, %QuickTransferInterval%
            }
        }
        
        IsTransferActive := false
    }
    
    return
}

﻿#Include, ./hotkeys/advanced/AdvancedSecondaryXP.ahk
#Include, ./hotkeys/advanced/AdvancedHerbalist.ahk
#Include, ./hotkeys/advanced/AdvancedAutoClick.ahk

HotkeyAdvancedFeaturesStart() {
  global Lang
  global AdvancedFeaturesTranslations
  
  global AdvancedFeatureSelected
  global AreAdvancedFeaturesToggle
  global AdvancedfeatureRunID
  global IsAdvancedFeatureRunning
  global ShortcutStopAdvanced

  if( AreAdvancedFeaturesToggle && !IsAdvancedFeatureRunning ) {

    IsAdvancedFeatureRunning := true
    AdvancedfeatureRunID++

    GuiControl, Toolbox:, AdvancedFeaturesIterations, 0
    GuiControl, Toolbox:, AdvancedFeatureRunning, % Lang.runningFeaturePrefix " - " AdvancedFeaturesTranslations[AdvancedFeatureSelected]

    if( AdvancedFeatureSelected == "secondaryXP" ){
      AdvancedSecondaryXP()
    }

    else if( AdvancedFeatureSelected == "herbalist" ){
      AdvancedHerbalist()
    }
   
    else if( AdvancedFeatureSelected == "autoClick" ){
      GuiControl, Toolbox:Disable, AutoClickTypeInput
      AdvancedAutoClick()
    }
   
  }

  return
}

HotkeyAdvancedFeaturesStop(){
  global AdvancedfeatureRunID
  global IsAdvancedFeatureRunning

  IsAdvancedFeatureRunning := false
  AdvancedfeatureRunID++

  GuiControl, Toolbox:Enable, AutoClickTypeInput
  GuiControl, Toolbox:, AdvancedFeatureRunning, 
  GuiControl, Toolbox:, AdvancedProgressbar, 0
}
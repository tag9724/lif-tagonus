﻿#Include, ./actions/SendDestack.ahk

;--------------------------------------------------------;
;Base destack

HotkeyDestack(){    
    global DestackAmount
    global ShortcutDestack

    SendDestack(DestackAmount, ShortcutDestack)
}


;--------------------------------------------------------;
;Statics optionnals destack

HotkeyStaticDestack1(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[1], ShortcutStaticDestacks[1])
}

HotkeyStaticDestack2(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[2], ShortcutStaticDestacks[2])
}

HotkeyStaticDestack3(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[3], ShortcutStaticDestacks[3])
}

HotkeyStaticDestack4(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[4], ShortcutStaticDestacks[4])
}

HotkeyStaticDestack5(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[5], ShortcutStaticDestacks[5])
}

HotkeyStaticDestack6(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[6], ShortcutStaticDestacks[6])
}

HotkeyStaticDestack7(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[7], ShortcutStaticDestacks[7])
}

HotkeyStaticDestack8(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[8], ShortcutStaticDestacks[8])
}

HotkeyStaticDestack9(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[9], ShortcutStaticDestacks[9])
}

HotkeyStaticDestack10(){    
    global StaticDestackAmounts
    global ShortcutStaticDestacks

    SendDestack(StaticDestackAmounts[10], ShortcutStaticDestacks[10])
}
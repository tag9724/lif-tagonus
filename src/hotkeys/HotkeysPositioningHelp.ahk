﻿HotkeyPosHelpLeft(){
  GuiControlGet, PositioningHelpLRInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX - PositioningHelpLRInput, MousePosY, 0

  return
}

HotkeyPosHelpRight(){
  GuiControlGet, PositioningHelpLRInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX + PositioningHelpLRInput, MousePosY, 0

  return
}

HotkeyPosHelpUp(){
  GuiControlGet, PositioningHelpUDInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX, MousePosY + PositioningHelpUDInput, 0

  return
}

HotkeyPosHelpDown(){
  GuiControlGet, PositioningHelpUDInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX, MousePosY - PositioningHelpUDInput, 0

  return
}

HotkeyPosHelpRotLeft(){
  GuiControlGet, PositioningHelpRotInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX - PositioningHelpRotInput, MousePosY, 0

  return
}

HotkeyPosHelpRotRight(){
  GuiControlGet, PositioningHelpRotInput, Toolbox:

  MouseGetPos, MousePosX, MousePosY
  MouseClickDrag, Left, MousePosX, MousePosY,  MousePosX + PositioningHelpRotInput, MousePosY, 0

  return
}
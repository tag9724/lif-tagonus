﻿#Include, ./actions/SendGameInteraction.ahk
#Include, ./hotkeys/HotkeysAdvancedFeatures.ahk

AdvancedAutoClick(){
  global AdvancedfeatureRunID

  global AutoClickInterval
  global AutoClickType

  global GameInteractionKey
  global GameSecondaryKey
  global GameTertiaryKey

  runID := AdvancedfeatureRunID

  ;Focus game window & hide Iterations count
  WinActivate, ahk_exe cm_client.exe
  GuiControl, Toolbox:, AdvancedFeaturesIterations,

  if( InStr( AutoClickType, "Key" ) ){

    interactionKey := GameInteractionKey

    if(AutoClickType == "secondaryKey"){
      interactionKey := GameSecondaryKey
    }

    if(AutoClickType == "tertiaryKey"){
      interactionKey := GameTertiaryKey
    }

    Loop, {

      if ( !WinExist("ahk_exe cm_client.exe") ) {
        HotkeyAdvancedFeaturesStop()
        return
      }

      if ( runID < AdvancedfeatureRunID ) {
        return
      }

      ControlFocus,,ahk_exe cm_client.exe
      ControlSend,,{Control down}%interactionKey%{Control up},ahk_exe cm_client.exe

      secondsInterval := AutoClickInterval / 1000
      waitIterations := 0

      Loop, % secondsInterval {

        if ( !WinExist("ahk_exe cm_client.exe") ) {
          HotkeyAdvancedFeaturesStop()
          return
        }

        if ( runID < AdvancedfeatureRunID ) {
          return
        }

        waitIterations++
        Sleep, 1000
      }

      lastDelay := secondsInterval - waitIterations

      if( lastDelay > 0 &&  runID == AdvancedfeatureRunID ){
        Sleep, % lastDelay * 1000
      }
    }
  }

  else {

    if( AutoClickType == "altClick" ){
      sendClick = {Alt down}{Click, left}{Alt up}
    }
    else if( AutoClickType == "ctrlClick" ){
      sendClick = {Control down}{Click, left}{Control up}
    }
    else if( AutoClickType == "dblClick" ){
      sendClick = {Click, left}{Click, left}
    }
    else {
      sendClick = {Click, left}
    }

    Loop, {
      
      if ( !WinExist("ahk_exe cm_client.exe") ) {
        HotkeyAdvancedFeaturesStop()
        return
      }

      if ( runID < AdvancedfeatureRunID ) {
        return
      }

      Send %sendClick%

      secondsInterval := AutoClickInterval / 1000
      waitIterations := 0

      Loop, % secondsInterval {

        if ( !WinExist("ahk_exe cm_client.exe") ) {
          HotkeyAdvancedFeaturesStop()
          return
        }

        if ( runID < AdvancedfeatureRunID ) {
          return
        }

        waitIterations++
        Sleep, 1000
      }

      lastDelay := secondsInterval - waitIterations

      if( lastDelay > 0 &&  runID == AdvancedfeatureRunID ){
        Sleep, % lastDelay * 1000
      }

    }
  }
}
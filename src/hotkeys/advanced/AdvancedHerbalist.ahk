﻿#Include, ./actions/SendGameInteraction.ahk
#Include, ./hotkeys/HotkeysAdvancedFeatures.ahk

AdvancedHerbalist(){
  global AdvancedfeatureRunID

  global HerbalistPreparationAmount
  global HerbalistPreparationCraftTime
  global HerbalistRepairTime

  runID := AdvancedfeatureRunID
  iterations := 0

  CoordMode, Mouse, Client

  Loop {
    GuiControl, Toolbox:, AdvancedProgressbar, 0


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }

    if( runID < AdvancedfeatureRunID ) {
      return
    }

    iterations++
    crafted := 0

    ;Start produce en masse
    if( runID == AdvancedfeatureRunID ){

      ;Get game window size & Focus
      WinActivate, ahk_exe cm_client.exe
      WinGetPos, wx, wy, width, height, A
      
      winSizeX := ( width / 2 )
      winSizeY := ( height / 2 )

      MouseClick, left, 150, 650

      Sleep, 50
      MouseClick, left, winSizeX + 4, winSizeY + 45
    }

    
    ;Wait for X seconds per preparation to craft
    Loop, %HerbalistPreparationAmount% {
            
      if ( !WinExist("ahk_exe cm_client.exe") ) {
        HotkeyAdvancedFeaturesStop()
        return
      }

      if( runID < AdvancedfeatureRunID ) {
        return
      }
          
      GuiControl, Toolbox:, AdvancedProgressbar, % crafted++ / HerbalistPreparationAmount * 94
      Sleep, % HerbalistPreparationCraftTime   
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }
    
    ;Cancel crafting
    if( runID == AdvancedfeatureRunID ){
      SendGameInteraction()
      Sleep, 500
      SendGameInteraction()
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }
    
    ;Use repair kit
    if( runID == AdvancedfeatureRunID ){
      
      ;Get game window size & Focus
      WinActivate, ahk_exe cm_client.exe
      WinGetPos, wx, wy, width, height, A
      
      winSizeX := ( width / 2 )
      winSizeY := ( height / 2 )

      MouseClick, left, winSizeX, winSizeY -250
      MouseClick, left, winSizeX, winSizeY -250

      GuiControl, Toolbox:, AdvancedProgressbar, 96
      Sleep, % HerbalistRepairTime
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }
    
    if( runID == AdvancedfeatureRunID ){
      GuiControl, Toolbox:, AdvancedProgressbar, 100
      GuiControl, Toolbox:, AdvancedFeaturesIterations, % iterations
      Sleep, 125
    }    
  }
}
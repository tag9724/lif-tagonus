﻿#Include, ./actions/SendGameInteraction.ahk
#Include, ./actions/SendGameValidation.ahk

#Include, ./hotkeys/HotkeysAdvancedFeatures.ahk

AdvancedSecondaryXP(){
  global AdvancedfeatureRunID
  global SecondaryXPIntervalAction
  global SecondaryXPIntervalAcceptPrompt

  runID := AdvancedfeatureRunID
  iterations := 0

  Loop {
    GuiControl, Toolbox:, AdvancedProgressbar, 0

    if( runID < AdvancedfeatureRunID ) {
      return
    }

    iterations++


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }

    if ( runID == AdvancedfeatureRunID ){
      SendGameInteraction()
      GuiControl, Toolbox:, AdvancedProgressbar, 20
      Sleep %SecondaryXPIntervalAction%
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }

    if ( runID == AdvancedfeatureRunID ){
      SendGameInteraction()
      GuiControl, Toolbox:, AdvancedProgressbar, 45
      Sleep %SecondaryXPIntervalAcceptPrompt%
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }

    if ( runID == AdvancedfeatureRunID ){
      SendGameValidation()
      GuiControl, Toolbox:, AdvancedProgressbar, 80
      Sleep %SecondaryXPIntervalAction%
    }


    if ( !WinExist("ahk_exe cm_client.exe") ) {
      HotkeyAdvancedFeaturesStop()
      return
    }

    if ( runID == AdvancedfeatureRunID ){
      GuiControl, Toolbox:, AdvancedProgressbar, 100
      GuiControl, Toolbox:, AdvancedFeaturesIterations, % iterations
      Sleep, 100
    }
  }


}
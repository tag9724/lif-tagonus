﻿#Include, ./utils/IsGameWindowActive.ahk
#Include, ./utils/GetGameRelativeCoords.ahk


DefineInventoryPosition(){

  global Lang
  global InventoryCoords
  
  ;Focus Game window
  WinActivate, ahk_exe cm_client.exe

  ; Wait for mouse to be pressed
  KeyWait, LButton, D

  ; Only set coords when game window is clicked
  if(IsGameWindowActive()){
      
      ; Get position based on mouse cursor
      GetGameRelativeCoords( Px, Py )

      ; Set new coordinates to use
      InventoryCoords[1] := Px
      InventoryCoords[2] := Py
      
      ; Save them to config file
      parsedCoords := Px "," Py
      IniWrite, %parsedCoords%, Tagonus-user-settings.ini, General, InventoryCoords

      ; Hack prevent moving ingame window
      KeyWait, LButton, U
      Sleep, 100

      MsgBox % Lang.inventoryPositionUpdated
  }

  ; User clicked outside game window
  else {
    MsgBox % Lang.inventoryPositionCannotUpdate
  }

  ; Focus Parameters window again
  Gui, Settings:Show

}
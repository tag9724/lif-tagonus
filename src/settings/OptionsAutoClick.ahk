﻿AutoClickIntervalChange(){
  global AutoClickInterval

  Sleep, 200
  GuiControlGet, AutoClickIntervalInput

  if( AutoClickIntervalInput < 1 ){
    AutoClickInterval := 1
  }

  else {
    AutoClickInterval := AutoClickIntervalInput
  }

  IniWrite, %AutoClickInterval%, Tagonus-user-settings.ini, AutoClick, Interval
}


AutoClickTypeChange(){
  global AutoClickType    
  global AdvancedAutoClickTranslations

  Sleep, 200

  GuiControlGet, AutoClickTypeInput
  clickType := ""

  for k, v in AdvancedAutoClickTranslations {
    if( AutoClickTypeInput == v) {
      clickType := k
    }
  }

  AutoClickType := clickType
  IniWrite, %AutoClickType%, Tagonus-user-settings.ini, AutoClick, Type
}
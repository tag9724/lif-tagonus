﻿HerbalistPreparationAmountChange(){
    global HerbalistPreparationAmount

    Sleep, 200
    GuiControlGet, HerbalistPreparationAmountInput

    if( HerbalistPreparationAmountInput < 1 ){
      HerbalistPreparationAmount := 1
    }

    else {
      HerbalistPreparationAmount := HerbalistPreparationAmountInput
    }

    IniWrite, %HerbalistPreparationAmount%, Tagonus-user-settings.ini, Herbalist, PreparationAmount
}

HerbalistPreparationCraftTimeChange(){
    global HerbalistPreparationCraftTime

    Sleep, 200
    GuiControlGet, HerbalistPreparationCraftTimeInput

    if( HerbalistPreparationCraftTimeInput < 50 ){
      HerbalistPreparationCraftTime := 50
    }

    else {
      HerbalistPreparationCraftTime := HerbalistPreparationCraftTimeInput
    }

    IniWrite, %HerbalistPreparationCraftTime%, Tagonus-user-settings.ini, Herbalist, PreparationCraftTime
}

HerbalistRepairTimeChange(){
    global HerbalistRepairTime

    Sleep, 200
    GuiControlGet, HerbalistRepairTimeInput

    if( HerbalistRepairTimeInput < 50 ){
      HerbalistRepairTime := 50
    }

    else {
      HerbalistRepairTime := HerbalistRepairTimeInput
    }

    IniWrite, %HerbalistRepairTime%, Tagonus-user-settings.ini, Herbalist, RepairTime
}

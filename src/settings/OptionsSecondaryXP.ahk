﻿SecondaryXPIntervalActionChange(){
    global SecondaryXPIntervalAction

    Sleep, 200
    GuiControlGet, SecondaryXPIntervalActionInput

    if( SecondaryXPIntervalActionInput < 500 ){
      SecondaryXPIntervalAction := 500
    }

    else {
      SecondaryXPIntervalAction := SecondaryXPIntervalActionInput
    }

    IniWrite, %SecondaryXPIntervalAction%, Tagonus-user-settings.ini, SecondaryXP, IntervalAction
}

SecondaryXPIntervalAcceptPromptChange(){
    global SecondaryXPIntervalAcceptPrompt

    Sleep, 200
    GuiControlGet, SecondaryXPIntervalAcceptPromptInput

    if( SecondaryXPIntervalAcceptPromptInput < 80 ){
      SecondaryXPIntervalAcceptPrompt := 80
    }
    
    else {
      SecondaryXPIntervalAcceptPrompt := SecondaryXPIntervalAcceptPromptInput
    }

    SecondaryXPIntervalAcceptPrompt := SecondaryXPIntervalAcceptPromptInput
    IniWrite, %SecondaryXPIntervalAcceptPrompt%, Tagonus-user-settings.ini, SecondaryXP, IntervalAcceptPrompt
}

﻿SaveAdvancedFeaturesState(){
  global AreAdvancedFeaturesToggle
  global AdvancedFeatureSelected

  IniWrite, %AreAdvancedFeaturesToggle%, Tagonus-user-settings.ini, States, AdvancedFeaturesToggle
  IniWrite, %AdvancedFeatureSelected%, Tagonus-user-settings.ini, States, AdvancedFeatureSelected

  return
}
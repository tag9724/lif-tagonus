﻿SavePositioningHelpState(){
  global IsPositioningHelpToggle

  IniWrite, %IsPositioningHelpToggle%, Tagonus-user-settings.ini, States, PositioningHelpToggle

  return
}
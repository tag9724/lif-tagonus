﻿#Include, ./GUI/toolbox/display/RefreshToolboxUI.ahk

SaveSettings(){

  global Lang

  shouldRefreshToolbox := false

  ;--------------------------------------------------------;
  ;Destack Shortcut

  global ShortcutDestack

  GuiControlGet, DestackShortcutInput,Settings:
  ShortcutDestack = %DestackShortcutInput%

  IniWrite, %DestackShortcutInput%, Tagonus-user-settings.ini, Shortcuts, Destack


  ;--------------------------------------------------------;
  ;Quick Transfer Shortcut

  global ShortcutQuickTransferClick

  GuiControlGet, QuickTransferClickShortcutInput,Settings:
  ShortcutQuickTransferClick = %QuickTransferClickShortcutInput%

  IniWrite, %QuickTransferClickShortcutInput%, Tagonus-user-settings.ini, Shortcuts, QuickTransferClick

  ;--------------------------------------------------------;
  ;Quick Tomb Transfer Shortcut

  global ShortcutQuickTombTransfer

  GuiControlGet, QuickTombTransferShortcutInput,Settings:
  ShortcutQuickTombTransfer = %QuickTombTransferShortcutInput%

  IniWrite, %QuickTombTransferShortcutInput%, Tagonus-user-settings.ini, Shortcuts, QuickTombTransfer


  ;--------------------------------------------------------;
  ;Focus Tagonus Shortcut

  global ShortcutFocusGUI

  GuiControlGet, FocusGUIShortcutInput,Settings:
  ShortcutFocusGUI = %FocusGUIShortcutInput%

  IniWrite, %FocusGUIShortcutInput%, Tagonus-user-settings.ini, Shortcuts, GUIFocus

  ;--------------------------------------------------------;
  ;Advanced features Game Action key

  global GameInteractionKey

  GuiControlGet, GameInteractionKeyInput,Settings:
  GameInteractionKey = %GameInteractionKeyInput%

  IniWrite, %GameInteractionKey%, Tagonus-user-settings.ini, General, GameInteractionKey

  ;--------------------------------------------------------;
  ;Advanced features Game Secondary key

  global GameSecondaryKey

  GuiControlGet, GameSecondaryKeyInput,Settings:
  GameSecondaryKey = %GameSecondaryKeyInput%

  IniWrite, %GameSecondaryKey%, Tagonus-user-settings.ini, General, GameSecondaryKey

  ;--------------------------------------------------------;
  ;Advanced features Game Tertiary key

  global GameTertiaryKey

  GuiControlGet, GameTertiaryKeyInput,Settings:
  GameTertiaryKey = %GameTertiaryKeyInput%

  IniWrite, %GameTertiaryKey%, Tagonus-user-settings.ini, General, GameTertiaryKey

  ;--------------------------------------------------------;
  ;Advanced features Start/Stop advanced feature

  global ShortcutStartAdvanced
  global ShortcutStopAdvanced

  GuiControlGet, StartAdvancedInput,Settings:
  GuiControlGet, StopAdvancedInput,Settings:

  ShortcutStartAdvanced = %StartAdvancedInput%
  IniWrite, %StartAdvancedInput%, Tagonus-user-settings.ini, Shortcuts, StartAdvanced

  ShortcutStopAdvanced = %StopAdvancedInput%
  IniWrite, %StopAdvancedInput%, Tagonus-user-settings.ini, Shortcuts, StopAdvanced

  if( StartAdvancedInput && !ShortcutStopAdvanced ){
    MsgBox % Lang.requireStopAdvanced
  }
  

  ;--------------------------------------------------------;
  ;Positioning Help

  global IsPositioningHelpEnabled
  GuiControlGet, EnablePositioningHelpInput,Settings:

  if ( IsPositioningHelpEnabled != EnablePositioningHelpInput ) { 
    shouldRefreshToolbox     := true
    IsPositioningHelpEnabled := EnablePositioningHelpInput

    IniWrite, %IsPositioningHelpEnabled%, Tagonus-user-settings.ini, PositioningHelp, PositioningHelpEnabled
  }

  ; This part look insane ... use a loop next time ...

  global ShortcutPosHelpLeft
  global ShortcutPosHelpRight
  global ShortcutPosHelpUp
  global ShortcutPosHelpDown
  global ShortcutPosHelpRotLeft
  global ShortcutPosHelpRotRight

  GuiControlGet, PosHelpLeftInput, Settings:
  GuiControlGet, PosHelpRightInput, Settings:
  GuiControlGet, PosHelpUpInput, Settings:
  GuiControlGet, PosHelpDownInput, Settings:
  GuiControlGet, PosHelpRotLeftInput, Settings:
  GuiControlGet, PosHelpRotRightInput, Settings:

  ShortcutPosHelpLeft     := PosHelpLeftInput
  ShortcutPosHelpRight    := PosHelpRightInput
  ShortcutPosHelpUp       := PosHelpUpInput
  ShortcutPosHelpDown     := PosHelpDownInput
  ShortcutPosHelpRotLeft  := PosHelpRotLeftInput
  ShortcutPosHelpRotRight := PosHelpRotRightInput

  IniWrite, %ShortcutPosHelpLeft%, Tagonus-user-settings.ini, Shortcuts, PosHelpLeft
  IniWrite, %ShortcutPosHelpRight%, Tagonus-user-settings.ini, Shortcuts, PosHelpRight
  IniWrite, %ShortcutPosHelpUp%, Tagonus-user-settings.ini, Shortcuts, PosHelpUp
  IniWrite, %ShortcutPosHelpDown%, Tagonus-user-settings.ini, Shortcuts, PosHelpDown
  IniWrite, %ShortcutPosHelpRotLeft%, Tagonus-user-settings.ini, Shortcuts, PosHelpRotLeft
  IniWrite, %ShortcutPosHelpRotRight%, Tagonus-user-settings.ini, Shortcuts, PosHelpRotRight


  ;--------------------------------------------------------;
  ;Static Destacks

  global StaticDestackAmounts
  global ShortcutStaticDestacks

  GuiControlGet, StaticDestackAmountInput1, Settings:
  GuiControlGet, StaticDestackAmountInput2, Settings:
  GuiControlGet, StaticDestackAmountInput3, Settings:
  GuiControlGet, StaticDestackAmountInput4, Settings:
  GuiControlGet, StaticDestackAmountInput5, Settings:
  GuiControlGet, StaticDestackAmountInput6, Settings:
  GuiControlGet, StaticDestackAmountInput7, Settings:
  GuiControlGet, StaticDestackAmountInput8, Settings:
  GuiControlGet, StaticDestackAmountInput9, Settings:
  GuiControlGet, StaticDestackAmountInput10, Settings:

  GuiControlGet, ShortcutStaticDestackInput1, Settings:
  GuiControlGet, ShortcutStaticDestackInput2, Settings:
  GuiControlGet, ShortcutStaticDestackInput3, Settings:
  GuiControlGet, ShortcutStaticDestackInput4, Settings:
  GuiControlGet, ShortcutStaticDestackInput5, Settings:
  GuiControlGet, ShortcutStaticDestackInput6, Settings:
  GuiControlGet, ShortcutStaticDestackInput7, Settings:
  GuiControlGet, ShortcutStaticDestackInput8, Settings:
  GuiControlGet, ShortcutStaticDestackInput9, Settings:
  GuiControlGet, ShortcutStaticDestackInput10, Settings:

  StaticDestackAmounts   := [  StaticDestackAmountInput1
                              ,StaticDestackAmountInput2
                              ,StaticDestackAmountInput3
                              ,StaticDestackAmountInput4
                              ,StaticDestackAmountInput5
                              ,StaticDestackAmountInput6
                              ,StaticDestackAmountInput7
                              ,StaticDestackAmountInput8
                              ,StaticDestackAmountInput9
                              ,StaticDestackAmountInput10 ]

  ShortcutStaticDestacks := [  ShortcutStaticDestackInput1
                              ,ShortcutStaticDestackInput2
                              ,ShortcutStaticDestackInput3
                              ,ShortcutStaticDestackInput4
                              ,ShortcutStaticDestackInput5
                              ,ShortcutStaticDestackInput6
                              ,ShortcutStaticDestackInput7
                              ,ShortcutStaticDestackInput8
                              ,ShortcutStaticDestackInput9
                              ,ShortcutStaticDestackInput10 ]


  Loop, 10 {
    amount   := StaticDestackAmounts[A_Index]
    shortcut := ShortcutStaticDestacks[A_Index]

    IniWrite, %amount%, Tagonus-user-settings.ini, StaticDestackAmounts, StaticDestack%A_Index%
    IniWrite, %shortcut%, Tagonus-user-settings.ini, Shortcuts, StaticDestack%A_Index%
  }


  ;--------------------------------------------------------;
  ;Interface Language

  global UserLanguage
  global Languages
  global Lang

  GuiControlGet, InterfaceLanguageInput,Settings:

  for k, v in Languages {

      ;Current language changed, save settings & restart Tagonus
      if( v.langName == InterfaceLanguageInput && k != UserLanguage ){
        IniWrite, %k%, Tagonus-user-settings.ini, General, Language
        
        Reload
        return
      }
  }

  if( shouldRefreshToolbox ){
    RefreshToolboxUI()
  }

  return
}
﻿SetDestackAmount(){
  global DestackAmount
  GuiControlGet, DestackAmountInput
  
  GuiControl, Text, DestackAmountLabel, %DestackAmountInput%
  DestackAmount := DestackAmountInput

  IniWrite, %DestackAmountInput%, Tagonus-user-settings.ini, General, DestackAmount
  return
}

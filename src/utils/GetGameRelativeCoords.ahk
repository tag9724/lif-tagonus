﻿GetGameRelativeCoords( ByRef X, ByRef Y )
{
    if( IsGameWindowActive() ) {

        ; Get Windows coords and size
        WinGetPos, wx, wy, width, height, A

        ; Get mouse position relative to game window
        MouseGetPos, mx, my

        X := mx - ( width / 2 )
        Y := my - ( height / 2 )

        return
    }
}
